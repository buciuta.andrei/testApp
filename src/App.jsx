import "./App.css";
import MainPage from "./Pages/MainPage";
import Store from "./Store/store";

// Main App - creates store and passes it to MainPage
function App() {
  const store = new Store();

  return <MainPage store={store} />;
}

export default App;
