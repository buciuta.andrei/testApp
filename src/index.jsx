import ReactDOM from "react-dom";
import { HashRouter as Router } from "react-router-dom";
import "./index.css";
import App from "./App";

// ReactJs biolerplate - loads main app and routing
ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById("root")
);
