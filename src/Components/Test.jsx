/* eslint-disable no-nested-ternary */
import { useEffect } from "react";
import { observer } from "mobx-react";
import { CSVLink } from "react-csv";
import { LinearProgress, Box } from "@mui/material";
import Circle from "./Circle";
import TriangleOccluder from "./Occluder";
// import occluder from "../assets/Occluder-min.png";
// import mirrorOccluder from "../assets/Occluder_Mirror-min.png";
import rectangleOccluder from "../assets/Rectangle-min.png";

const Test = ({ store }) => {
  // The function called on keypress
  const handleKeyPress = (event) => {
    if (!store.isStartTest) {
      store.startTest();
      return;
    }

    if (event.code === store.matchKey || event.code === store.noMatchKey) {
      store.endTest(event.code);
      return;
    }

    if (event.code === "Equal") {
      store.concludeTrial();
    }
  };

  // binds keypress to a function only at the first setup of the <Test> component
  useEffect(() => {
    document.addEventListener("keydown", handleKeyPress);
  }, []);

  const occluderRotation = store.reversedOccluder
    ? "rotate(90deg)"
    : "rotate(0deg)";

  // Defined separate elements of the Test Page using JSX
  // The 2 circles with 2 <Circle> instances
  const circleElement = (
    <div
      style={{
        transform: `rotate(${store.rotationDegrees}deg) scale(0.4)`,
        height: "100%",
      }}
    >
      {store.occluderType === "bl0" ? (
        <div />
      ) : store.occluderType === "bl1" ? (
        <TriangleOccluder
          topPosition="50%"
          occluderRotation={occluderRotation}
        />
      ) : (
        <img src={rectangleOccluder} alt="occluder" />
      )}
      <Circle circleId="L" store={store} />
      <Circle circleId="R" store={store} />
    </div>
  );

  // What to show when the trials concluded
  const concludedElement = (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        textAlign: "center",
        alignItems: "center",
        justifyContent: "center",
        height: "100%",
      }}
    >
      <h2>Trial concluded. Thank you.</h2>
      <h2>
        Your secret completion code is <strong>{store.idCode}</strong>
      </h2>
      <CSVLink
        type="button"
        style={{ width: "fit-content", height: "2rem", margin: "2rem" }}
        data={store.resultsBuffer}
      >
        DOWNLOAD RESULTS
      </CSVLink>
    </div>
  );

  let testPhaseString = "";

  if (store.testNo === 0 && store.isTraining) {
    testPhaseString = "Let's practice!";
  }

  if (store.testNo === 0 && store.isFirstTestBlock) {
    testPhaseString =
      "Let's start the trial! \n Remember! Respond as quickly and accurately as you can!";
  }

  let responseString = `The response is INCORRECT. \n\nRemember! \nPress ${store.matchKey.slice(
    -1
  )} for same letter \nPress ${store.noMatchKey.slice(
    -1
  )} for different letter`;
  const correctMessage = `The response is CORRECT`;

  if (store.isTraining && store.isGotMatch) {
    responseString = correctMessage;
  }

  if ((!store.isTraining && store.isGotMatch) || store.testNo === 0) {
    responseString = "";
  }

  const continueString = "Press ANY KEY to continue";

  // What to show when the current test ended
  const endTestElement = (
    <div
      style={{
        height: "800px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        textAlign: "center",
      }}
    >
      <div>
        <h2 style={{ display: testPhaseString === "" ? "none" : "block" }}>
          {testPhaseString}
        </h2>
        <h2 style={{ display: responseString === "" ? "none" : "block" }}>
          {responseString}
        </h2>
        <h2 style={{ display: continueString === "" ? "none" : "block" }}>
          {continueString}
        </h2>
      </div>
      {store.isTraining ? (
        <div />
      ) : (
        <Box sx={{ width: "75%", position: "absolute", bottom: "20vh" }}>
          <LinearProgress
            variant="determinate"
            value={store.currentPercentFinished}
            sx={{ height: "8px" }}
          />
          {store.trialNumber === 85 ? (
            <p>
              Great job, you are already 33% of the way. Feel free to take a
              short break before you continue
            </p>
          ) : store.trialNumber === 169 ? (
            <p>
              Great job, you are already 66% of the way. Feel free to take a
              short break before you continue
            </p>
          ) : (
            ""
          )}
        </Box>
      )}
    </div>
  );

  const excludedElement = (
    <div
      style={{
        height: "100%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        textAlign: "center",
      }}
    >
      <h2>You do not mett the inclusion criteria.</h2>
      <h2>Thank you for participating.</h2>
    </div>
  );

  // Return elements depending on - is test started, is trial concluded - using ternary operators that are nested
  // Some elements have changeable parameters defined and processed by the Store

  return (
    <div
      style={{
        width: "800px",
        height: "800px",
        margin: "auto",
        position: "relative",
        padding: 0,
        fontSize: "16px",
      }}
    >
      {store.isExcludedFromTrial
        ? excludedElement
        : store.isTrialConcluded
        ? concludedElement
        : store.isStartTest && store.occluderType !== undefined
        ? circleElement
        : endTestElement}
    </div>
  );
};

export default observer(Test);
