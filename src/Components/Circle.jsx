/* eslint-disable no-unused-vars */
import { useSpring, animated } from "react-spring";
import { observer } from "mobx-react";

const Circle = ({ circleId, store }) => {
  // Show/HideLetter animation duration
  const diplayLetterAnimDuration = 250;

  // Delay from start to ShowLetter animation
  const showLetterDelay = 500;

  // Delay from start to HideLetter animation
  const hideLetterDelay = 500;

  // Same with position
  const positionValue = circleId === "R" ? "100%" : "0%";

  // Define the end point of the vertical movement depending on occluder and circle
  let verticalMovement;

  let diagonalMovement;

  if (store.reversedOccluder) {
    verticalMovement = circleId === "L" ? { top: "0%" } : { top: "100%" };
  } else {
    verticalMovement = circleId === "L" ? { top: "100%" } : { top: "0%" };
  }

  if (store.reversedOccluder) {
    diagonalMovement = circleId === "L" ? { top: "32%" } : { top: "68%" };
  } else {
    diagonalMovement = circleId === "L" ? { top: "68%" } : { top: "32%" };
  }

  // Animation chain
  // In the last anim - letter appears in each circle
  //  - sets the start time for reaction test
  //  - checks if the letters were modified - if not it modifies them according to the test parameters
  const [complexAnim, complexApi] = useSpring(() => ({
    immediate: true,
    config: { duration: 600 },
    to: async (next, cancel) => {
      await next({
        config: { duration: diplayLetterAnimDuration },
        delay: showLetterDelay,
        color: "black",
      });
      await next({
        config: { duration: diplayLetterAnimDuration },
        delay: hideLetterDelay,
        color: "white",
      });
      await next({
        left: circleId === "L" ? "32%" : "68%",
      });
      await next({
        left: "50%",
        ...diagonalMovement,
        config: { duration: 500 },
      });
      await next({ ...verticalMovement });
      await next({
        config: { duration: 0 },
        color: "black",
        onStart: () => {
          store.setStartingRecordedTime();
          if (!store.isLettersChanged) {
            store.changeLetters();
          }
        },
      });
    },
    from: {
      left: positionValue,
      top: "50%",
      opacity: 1,
      color: "white",
    },
  }));

  // Define base circle style using inline parameters not CSS (idk why)
  let circleStyle = {
    position: "absolute",
    display: "flex",
    border: "4px solid black",
    borderRadius: "50%",
    width: 109,
    height: 109,
    transform: "translate(-50%, -50%)",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 64,
    color: "white",
  };

  // if Circle is R - add left parameter to previous one - array destructuring again
  if (circleId === "R") {
    circleStyle = {
      ...circleStyle,
      left: "100%",
    };
  }

  // Show circle using animated.div from the animation package
  // Add counter-rotation to the letter so it stays in the normal position
  // Set circle letter from Store
  return (
    <animated.div style={{ ...circleStyle, ...complexAnim }}>
      <p
        style={{
          transform: `rotate(-${store.rotationDegrees}deg)`,
          fontWeight: "bold",
        }}
      >
        {circleId === "L" ? store.circleLLetter : store.circleRLetter}
      </p>
    </animated.div>
  );
};

export default observer(Circle);
