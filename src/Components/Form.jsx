/* eslint-disable react/jsx-boolean-value */
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";

// Form - basically that - using ReactHookForm package for ease of use
const Form = ({ store }) => {
  // Accesing history
  const history = useHistory();
  // Accessing some functions from the ReactHookForm package using array destructuring (intermediate javascript)
  const { register, handleSubmit, reset } = useForm();

  const onSubmitForm = (data) => {
    console.log(data);
    // DO SOMETHING with the form data - send to server
    store.setMTurkId(data.mturkid);
    store.setDominantHand(data.dominanthand);

    // Reset the form - clear values
    reset();

    // Switch to next page - Inform
    history.push("/inform");
  };

  // The form using the input type submit (last line) - calls the function declared in the <form> component
  // HandleSubmit on clicking the button calls the onSubmitForm and automatically passes a parameter (called data in this case)
  return (
    <Card sx={{ p: "24px", boxShadow: "none" }}>
      <form
        className="mainForm"
        onSubmit={handleSubmit(onSubmitForm)}
        style={{
          width: "fit-content",
          height: "fit-content",
        }}
      >
        <CardContent sx={{ display: "flex", flexDirection: "column" }}>
          <h1 style={{ textAlign: "center" }}>Required data:</h1>
          <input
            placeholder="MTurk ID"
            {...register("mturkid", { required: true })}
          />
          <select
            {...register("dominanthand", { required: true })}
            style={{
              height: "36px",
              width: "140px",
              margin: "1rem",
              padding: "0.5rem",
            }}
          >
            <option value="">Dominant hand</option>
            <option value="Left">Left</option>
            <option value="Right">Right</option>
            <option value="Neither">Neither</option>
          </select>
        </CardContent>
        <CardActions>
          <Button
            id="submitbutton"
            type="submit"
            size="medium"
            variant="contained"
            fullWidth
          >
            Submit
          </Button>
        </CardActions>
      </form>
    </Card>
  );
};

export default Form;
