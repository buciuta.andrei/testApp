/* eslint-disable no-unused-vars */
import { useEffect } from "react";
import { useSpring, animated } from "react-spring";
import { observer } from "mobx-react";

const Square = ({ setFinishedAnim }) => {
  // Show/HideLetter animation duration
  const diplayLetterAnimDuration = 250;

  // Delay from start to ShowLetter animation
  const showLetterDelay = 500;

  // Delay from start to HideLetter animation
  const hideLetterDelay = 500;

  // Change translate value depending on which circle is this instance R or L

  // Animation chain
  // In the last anim - letter appears in each circle
  //  - sets the start time for reaction test
  //  - checks if the letters were modified - if not it modifies them according to the test parameters
  const [complexAnim, complexApi] = useSpring(() => ({
    pause: true,
    config: { duration: 1600 },
    to: async (next, cancel) => {
      await next({
        left: "65%",
        onRest: () => {
          setFinishedAnim(true);
        },
      });
    },
    from: {
      left: "35%",
    },
  }));

  const startAnim = () => {
    complexApi.resume();
  };

  useEffect(() => {
    document.addEventListener("keydown", startAnim);
    setFinishedAnim(false);

    return function cleanup() {
      document.addEventListener("keydown", startAnim);
    };
  }, []);

  // Define base circle style using inline parameters not CSS (idk why)
  const squareStyle = {
    position: "absolute",
    display: "flex",
    width: 75,
    height: 75,
    justifyContent: "center",
    alignItems: "center",
    fontSize: 64,
    backgroundColor: "green",
    transform: "translate(-50%, -50%)",
  };

  // Show circle using animated.div from the animation package
  // Add counter-rotation to the letter so it stays in the normal position
  // Set circle letter from Store
  return <animated.div style={{ ...squareStyle, ...complexAnim }} />;
};

export default observer(Square);
