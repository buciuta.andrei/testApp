/* eslint-disable no-unused-vars */
import largeSq from "../assets/largeSq.png";
import smallSq from "../assets/smallSq.png";
import triangleR from "../assets/triangleR.png";
import triangleL from "../assets/triangleL.png";

const TriangleOccluder = ({ topPosition, occluderRotation, occluderScale }) => {
  let transformString = "translate(-50%, -50%)";

  if (occluderRotation !== undefined) {
    transformString = `translate(-50%, -50%) ${occluderRotation}`;
  }

  if (occluderScale !== undefined) {
    transformString += occluderScale;
  }

  return (
    <div
      style={{
        width: "432px",
        height: "432px",
        position: "absolute",
        transform: transformString,
        top: topPosition,
        left: "50%",
        zIndex: "10",
      }}
    >
      <div
        style={{
          position: "absolute",
          bottom: "0",
          display: "flex",
          borderStyle: "solid",
          borderWidth: "378px 0 0 378px",
          borderColor: "transparent transparent transparent #936f4c",
          width: 0,
          height: 0,
        }}
      />
      <div
        style={{
          position: "absolute",
          right: "0",
          display: "flex",
          borderStyle: "solid",
          borderWidth: "0 378px 378px 0",
          borderColor: "transparent #936f4c transparent transparent",
          width: 0,
          height: 0,
        }}
      />
    </div>
  );
};

export default TriangleOccluder;
