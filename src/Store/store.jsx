/* eslint-disable func-names */
/* eslint-disable no-unused-vars */
/* eslint-disable prefer-destructuring */
import { runInAction, makeAutoObservable } from "mobx";
import { getTime, formatISO, isThisSecond } from "date-fns";
import { readRemoteFile } from "react-papaparse";
import { shuffle } from "underscore";
import ksuid from "ksuid";
import axios from "axios";
import testListCsv from "../assets/tests.csv";

class Store {
  // VARIABLES SECTOR
  mTurkId = "";

  idCode = "";

  dominantHand = "";

  matchKey = "KeyJ";

  noMatchKey = "KeyF";

  usedLetterList = ["K", "M", "P", "S", "T", "V"];

  letterCombinations = [];

  rotationDegrees = 0;

  reversedOccluder = false;

  circleLLetter = "";

  circleLInitialLetter = "";

  circleRLetter = "";

  circleRInitialLetter = "";

  thirdLetter = "";

  initialTime = 0;

  keyPressTime = 0;

  testNo = 0;

  isTraining = true;

  isFirstTestBlock = false;

  isLettersChanged = false;

  isTestEnded = false;

  isStartTest = false;

  isTrialConcluded = false;

  isTestBlocksSet = false;

  isExcludedFromTrial = false;

  isTrainingPassed = false;

  reactionTimes = [];

  lastReactionTime;

  lastReactionTimeString = "";

  loadedTests = [];

  tests = [];

  firstTestSet = [];

  secondTestSet = [];

  trainingTestSet = [];

  trainingResults = [];

  isGotMatch;

  occluderType;

  occluderOrientation;

  letterMatchType;

  circleShowLetter;

  resultsBuffer = "";

  // this. will be used frequently - it means use the variable/function in this certain instance as the Store is not a standalone component and has to be constructed as an instance (intermediate OOP)

  // constructor - a set of functions that trigger when the store is first constructed
  constructor() {
    // Store boilerplate - makes the variables Observables - when they change the parts of the app that use them automatically change
    makeAutoObservable(this, {}, { deep: false });
    // reads the CSV file with the tests
    readRemoteFile(testListCsv, {
      // on completion use the results from the read and attribute them to the TESTS variable
      complete: (results) => {
        runInAction(() => {
          this.loadedTests = results.data;
        });
      },
    });
  }

  setMTurkId(mTurkId) {
    this.mTurkId = mTurkId;
  }

  setDominantHand(hand) {
    this.dominantHand = hand;
    this.idCode = ksuid.randomSync().string;

    const stringArray = [...this.idCode];

    stringArray.forEach(function (char, index) {
      switch (char) {
        case "1":
          stringArray[index] = "2";
          break;
        case "0":
          stringArray[index] = "3";
          break;
        case "o":
          stringArray[index] = "r";
          break;
        case "O":
          stringArray[index] = "R";
          break;
        case "l":
          stringArray[index] = "p";
          break;
        case "I":
          stringArray[index] = "K";
          break;
        default:
          break;
      }
    });

    this.idCode = stringArray.join("");

    this.setUpTestBlocks();
  }

  randomizeKeys() {
    this.matchKey = "KeyJ";
    this.noMatchKey = "KeyF";

    if (this.dominantHand === "Left") {
      this.matchKey = "KeyF";
      this.noMatchKey = "KeyJ";
    } else if (this.dominantHand === "Neither") {
      if (this.getRandomNumber(0, 1) === 0) {
        this.matchKey = "KeyF";
        this.noMatchKey = "KeyJ";
      }
    }
  }

  get reactionTime() {
    return this.keyPressTime - this.initialTime;
  }

  get trialNumber() {
    let trialNo = this.testNo;

    if (!this.isTraining && !this.isFirstTestBlock) {
      trialNo += this.firstTestSet.length;
    }

    return trialNo;
  }

  get totalTestNumber() {
    return this.firstTestSet.length + this.secondTestSet.length;
  }

  get currentPercentFinished() {
    return (this.trialNumber / this.totalTestNumber) * 100;
  }

  setupLetterCombinationsArray() {
    this.letterCombinations = this.usedLetterList.flatMap((v, i) =>
      this.usedLetterList.slice(i + 1).map((w) => v + w)
    );
  }

  setUpTestBlocks() {
    // If the test block are already defined, skip
    if (this.isTestBlocksSet) {
      return;
    }

    this.randomizeKeys();

    this.setupLetterCombinationsArray();

    const tempArray = [];
    let currentLetterCombination = [];

    this.loadedTests.forEach((test) => {
      currentLetterCombination = shuffle(this.letterCombinations);
      // eslint-disable-next-line no-plusplus
      for (let index = 0; index < 8; index++) {
        tempArray.push([...test, currentLetterCombination[index]]);
      }
    });

    this.tests = tempArray;

    Object.seal(this.tests);

    let firstBlock;
    let secondBlock;

    // Get a random number from 0 and 1 and depending on it choose the first block to start with
    if (this.getRandomNumber(0, 1) === 0) {
      firstBlock = "bl0";
      secondBlock = "bl1";
    } else {
      firstBlock = "bl1";
      secondBlock = "bl0";
    }

    // Filter the tests and divide them in the two blocks (filter - gets each variable from the array and adds it in another array only if the condition returns TRUE)
    this.firstTestSet = this.tests.filter((test) => test[0] === firstBlock);
    this.secondTestSet = this.tests.filter((test) => test[0] === secondBlock);

    // Set that the tests are setup
    this.isTestBlocksSet = true;

    // Make sure that the trialConcluded variable is false
    this.isTrialConcluded = false;

    // Randomize the order in the arrays
    this.firstTestSet = shuffle(this.firstTestSet);
    this.secondTestSet = shuffle(this.secondTestSet);

    const tempTrainingArray = [];

    tempTrainingArray.push(...shuffle(this.firstTestSet).slice(0, 5));
    tempTrainingArray.push(...shuffle(this.secondTestSet).slice(0, 5));

    const tempTestSet = shuffle(this.secondTestSet).slice(5, 25);

    const tempInterleavedArray = shuffle(this.firstTestSet)
      .slice(5, 25)
      .map((k, i) => [k, tempTestSet[i]])
      .flat();

    tempTrainingArray.push(...tempInterleavedArray);

    this.trainingTestSet = tempTrainingArray;

    this.setInitialValues();
  }

  setInitialValues() {
    this.initialTime = 0;
    this.keyPressTime = 0;
    this.selectTestFromList();
  }

  selectTestFromList() {
    // Select the test from the list
    if (this.isTraining) {
      this.currentTestArray = this.trainingTestSet[this.testNo];
    } else if (this.isFirstTestBlock) {
      this.currentTestArray = this.firstTestSet[this.testNo];
    } else {
      this.currentTestArray = this.secondTestSet[this.testNo];
    }

    // Sets a random rotation
    this.rotationDegrees = 0; // this.getRandomNumber(0, 175);

    // Applies the specific test parameters
    this.applyTestParameters();
  }

  applyTestParameters() {
    // Sets the specific parameters from the ex-CSV table columns
    this.occluderType = this.currentTestArray[0];
    this.occluderOrientation = this.currentTestArray[1];
    this.letterMatchType = this.currentTestArray[2];
    this.circleShowLetter = this.currentTestArray[3];

    if (this.occluderOrientation === "lev") {
      this.reversedOccluder = true;
    } else {
      this.reversedOccluder = false;
    }

    this.setInitialLetters();
  }

  setInitialLetters() {
    this.isLettersChanged = false;

    // sets the letters on the circles and saves them in separate variables for later comparisson
    this.circleLLetter = this.currentTestArray[4].split("")[0];
    this.circleLInitialLetter = this.circleLLetter;
    this.circleRLetter = this.currentTestArray[4].split("")[1];
    this.circleRInitialLetter = this.circleRLetter;
  }

  changeLetters() {
    this.isLettersChanged = true;

    this.thirdLetter = "";

    // Switch depending on the parameter

    switch (this.letterMatchType) {
      case "nm":
        this.noMatchScenario();
        break;
      case "mc":
        this.matchCongruentScenario();
        break;
      case "mi":
        this.matchIncongruentScenario();
        break;
      case "m":
        this.matchCongruentScenario();
        break;
      default:
        break;
    }
  }

  noMatchScenario() {
    this.thirdLetter = this.getUnusedCharacter();

    if (this.circleShowLetter === "d2") {
      this.circleLLetter = " ";
      this.circleRLetter = this.thirdLetter;
    } else {
      this.circleRLetter = " ";
      this.circleLLetter = this.thirdLetter;
    }
  }

  matchCongruentScenario() {
    if (this.circleShowLetter === "d2") {
      this.circleLLetter = " ";
      this.thirdLetter = this.circleRLetter;
    } else {
      this.circleRLetter = " ";
      this.thirdLetter = this.circleLLetter;
    }
  }

  matchIncongruentScenario() {
    this.circleRLetter = this.circleLInitialLetter;
    this.circleLLetter = this.circleRInitialLetter;

    if (this.circleShowLetter === "d2") {
      this.circleLLetter = " ";
      this.thirdLetter = this.circleRLetter;
    } else {
      this.circleRLetter = " ";
      this.thirdLetter = this.circleLLetter;
    }
  }

  getRandomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  getUnusedCharacter() {
    // Gets an array of unused letters using filter
    const unusedCharacterList = [...this.usedLetterList].filter(
      (letter) =>
        letter !== this.circleLInitialLetter &&
        letter !== this.circleRInitialLetter
    );

    // Gets random letter using the random function;
    // Math.random returns a number between 0 and 1 (eg. 0.454677), Math.floor round it to the nearest integer (0 or 1)
    let chosenLetter;

    if (Math.floor(Math.random()) === 0) {
      chosenLetter =
        unusedCharacterList[
          this.getRandomNumber(0, unusedCharacterList.length - 1)
        ];
    } else if (Math.floor(Math.random()) === 0) {
      chosenLetter = this.circleLInitialLetter;
    } else {
      chosenLetter = this.circleRInitialLetter;
    }

    return chosenLetter;
  }

  setStartingRecordedTime() {
    this.initialTime = getTime(new Date());
  }

  setKeyPress(pressedKey) {
    // checks if the key pressed is NoMatch and the test parameter is NoMatch
    if (pressedKey === this.noMatchKey && this.letterMatchType === "nm") {
      this.isGotMatch = true;
      // else checks if the Match key is pressend and the test parameters indicate matching letters
    } else if (
      pressedKey === this.matchKey &&
      (this.letterMatchType === "m" ||
        this.letterMatchType === "mi" ||
        this.letterMatchType === "mc")
    ) {
      this.isGotMatch = true;
      // Else the test subject is dead wrong
    } else {
      this.isGotMatch = false;
    }

    // Gets the current time in ms
    const currentTime = getTime(new Date());

    // If no other previous keys were pressed do
    if (this.keyPressTime === 0) {
      this.keyPressTime = currentTime;

      // Combine test parameters in one string
      const letterCombination = `${this.circleLInitialLetter}${this.circleRInitialLetter}${this.thirdLetter}`;
      const testName = `${this.occluderType}_${this.occluderOrientation}_${
        this.letterMatchType
      }_${this.circleShowLetter}_${letterCombination}_${this.rotationDegrees}${
        this.isTraining ? "_practice" : ""
      }`;

      // Set the variable for the excel table
      this.lastReactionTime = [
        this.idCode,
        this.mTurkId,
        this.dominantHand,
        testName,
        this.reactionTime,
        this.isGotMatch,
        this.dominantHand,
        this.matchKey,
        this.noMatchKey,
      ];

      // Get a string to show in the test end screen
      this.lastReactionTimeString = `${this.isGotMatch}`;

      // Add the last reaction time to the array that hold all the current trial results
      this.reactionTimes.push(this.lastReactionTime);

      if (this.isTraining) {
        this.trainingResults.push(this.isGotMatch);
        this.isTrainingPassed =
          this.trainingResults.length > 4
            ? !this.trainingResults.slice(-5).includes(false)
            : false;
      }

      // Save the test result to a buffer
      this.saveToBuffer();

      this.incrementTestNo();
    }
  }

  incrementTestNo() {
    if (this.isTraining && this.testNo === 9) {
      this.testNo = 0;
      this.isTraining = false;
      this.isFirstTestBlock = true;
    } else if (
      this.testNo === this.trainingTestSet.length - 1 &&
      this.isTraining
    ) {
      this.isExcludedFromTrial = true;
    } else if (
      this.testNo === this.firstTestSet.length - 1 &&
      this.isFirstTestBlock
    ) {
      this.testNo = 0;
      this.isFirstTestBlock = false;
    } else if (
      this.testNo === this.secondTestSet.length - 1 &&
      !this.isFirstTestBlock &&
      !this.isTraining
    ) {
      this.isTrialConcluded = true;
      this.concludeTrial();
      return;
    } else {
      this.testNo += 1;
    }

    this.setInitialValues();
  }

  startTest() {
    this.isTestEnded = false;
    this.isStartTest = true;
  }

  endTest(pressedKey) {
    this.isTestEnded = true;

    if (this.initialTime !== 0) {
      this.isStartTest = false;

      // Call the key press function
      this.setKeyPress(pressedKey);
    }
  }

  async saveToBuffer() {
    this.resultsBuffer += `${this.lastReactionTime.toString()}\r\n`;
  }

  async concludeTrial() {
    this.isStartTest = false;
    // Download Excel file with all the trial tests results and concludes the trial
    this.isTrialConcluded = true;

    this.saveCsvToServer();
  }

  saveCsvToServer() {
    const address =
      "http://pclpsrescit2.services.brown.edu/blt_lab/MP/data/tabletstudysave.php";
    axios
      .post(address, {
        submitDate: `${formatISO(Date.now(), { format: "basic" })}`,
        idCode: this.idCode,
        postresult_string: this.resultsBuffer,
      })
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  }
}

export default Store;
