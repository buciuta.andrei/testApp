/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Button,
} from "@mui/material";
import Square from "../Components/Square";
import Triangle from "../Components/Triangle";
import singleOccluder from "../assets/largeSq.png";
import dualOccluder from "../assets/Occluder.png";
import TriangleOccluder from "../Components/Occluder";

const Instructions = ({ store }) => {
  const [page, setPage] = useState(1);
  const [finishedAnim, setFinishedAnim] = useState(false);

  const history = useHistory();

  useEffect(() => {
    if (page > 5) {
      history.replace("/test");
    }
  }, [page]);

  let component;

  switch (page) {
    case 1:
      component = (
        <Card sx={{ height: "60vh", width: "50vw", boxShadow: "none" }}>
          <CardHeader
            title="Instructions"
            titleTypographyProps={{ variant: "h4" }}
          />
          <CardContent sx={{ textAlign: "center", fontSize: "18px" }}>
            <p>
              {`In the current study you will see objects pass behind covers. \n Sometimes there is a single cover and sometimes there are two. \n Here is an example with one cover.`}
            </p>
          </CardContent>
          <CardActions sx={{ paddingLeft: "10vw", paddingRight: "10vw" }}>
            <Button
              variant="contained"
              size="large"
              fullWidth
              onClick={() => setPage(2)}
            >
              NEXT
            </Button>
          </CardActions>
        </Card>
      );
      break;
    case 2:
      component = (
        <>
          <p
            style={{
              fontWeight: "bold",
              fontSize: "18px",
              textAlign: "center",
            }}
          >
            Press any button to start animation
          </p>
          <div style={{ height: "25vh" }} />
          <img
            src={singleOccluder}
            alt="singleOccluder"
            style={{
              position: "absolute",
              height: "25vh",
              transform: "translate(-50%, -50%)",
              left: "50%",
              zIndex: "100",
            }}
          />
          <Square setFinishedAnim={setFinishedAnim} />
          <div style={{ height: "20vh" }} />
          <div sx={{ paddingLeft: "10vw", paddingRight: "10vw" }}>
            <Button
              variant="contained"
              size="large"
              fullWidth
              onClick={() => setPage(3)}
              sx={{
                visibility: finishedAnim ? "visible" : "hidden",
              }}
            >
              NEXT
            </Button>
          </div>
        </>
      );
      break;
    case 3:
      component = (
        <Card
          elevation={8}
          sx={{ height: "60vh", width: "50vw", boxShadow: "none" }}
        >
          <CardHeader
            title="Instructions"
            titleTypographyProps={{ variant: "h4" }}
          />
          <CardContent sx={{ textAlign: "center", fontSize: "18px" }}>
            <p>Here is an example with two covers.</p>
          </CardContent>
          <CardActions sx={{ paddingLeft: "10vw", paddingRight: "10vw" }}>
            <Button
              variant="contained"
              size="large"
              fullWidth
              onClick={() => setPage(4)}
            >
              NEXT
            </Button>
          </CardActions>
        </Card>
      );
      break;
    case 4:
      component = (
        <div>
          <p
            style={{
              fontWeight: "bold",
            }}
          >
            Press any button to start animation
          </p>
          <div
            style={{
              height: "25vh",
              textAlign: "left",
              display: "flex",
            }}
          >
            <TriangleOccluder topPosition="50%" occluderScale="scale(0.5)" />
          </div>
          <Triangle setFinishedAnim={setFinishedAnim} />
          <div style={{ height: "20vh" }} />
          <Button
            variant="contained"
            size="large"
            fullWidth
            onClick={() => setPage(5)}
            sx={{
              visibility: finishedAnim ? "visible" : "hidden",
            }}
          >
            NEXT
          </Button>
        </div>
      );
      break;
    case 5:
      component = (
        <Card
          elevation={8}
          sx={{
            height: "60vh",
            width: "50vw",
            boxShadow: "none",
            overflowY: "scroll",
          }}
        >
          <CardHeader
            title="Instructions"
            titleTypographyProps={{ variant: "h4" }}
          />
          <CardContent sx={{ textAlign: "center", fontSize: "18px" }}>
            <p>
              In the videos you will see two discs with a letter presented on
              both of them. These letters will disappear. Later another letter
              will reappear on one of the discs. <br />
              <br /> Your task will be to decide whether the letter that appears
              at the end was the same as EITHER of the ones that was shown in
              the beginning. <br />
              <br /> If you recognize the letter from the beginning of the trial
              press <strong>{store.matchKey.slice(-1)}</strong>. <br />
              <br /> If the letter was NOT one of the initial ones, press{" "}
              <strong>{store.noMatchKey.slice(-1)}</strong>. <br />
              <br /> <strong>Be as fast and accurate as you can.</strong> We are
              interested in how quickly you can respond while still being
              accurate.
            </p>
          </CardContent>
          <CardActions sx={{ paddingLeft: "10vw", paddingRight: "10vw" }}>
            <Button variant="contained" fullWidth onClick={() => setPage(6)}>
              NEXT
            </Button>
          </CardActions>
        </Card>
      );
      break;
    default:
      component = null;
      break;
  }

  return component;
};

export default Instructions;
