/* eslint-disable no-unused-vars */
import { Switch, Route } from "react-router-dom";
import Form from "../Components/Form";
import Inform from "./Inform";
import Test from "../Components/Test";
import "./MainPage.css";
import Instructions from "./Instructions";
import TriangleOccluder from "../Components/Occluder";

// MainPage body - gets Store parameter from App.jsx
const MainPage = ({ store }) => {
  // history - navigation utility included in React Router (React Router - page switching package)

  // Page Body
  // Switch indicates the part of the app that is changeable using React Router
  // Route - the path that holds a certain Page a.k.a. Component - Form, Inform, Test, etc
  // Store is passed to the test component via parameters
  return (
    <div>
      <Switch>
        <Route path="/" exact>
          <Form store={store} />
        </Route>
        <Route path="/inform">
          <Inform />
        </Route>
        <Route path="/instructions">
          <Instructions store={store} />
        </Route>
        <Route path="/test">
          <Test store={store} />
        </Route>
      </Switch>
    </div>
  );
};

export default MainPage;
