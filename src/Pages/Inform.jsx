/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { NavLink, useHistory } from "react-router-dom";
import Button from "@mui/material/Button";
import { Card, CardActions, CardContent, CardHeader } from "@mui/material";

// Just the inform page with history
const Inform = () => {
  const history = useHistory();

  return (
    <Card
      className="print"
      sx={{
        width: "50vw",
        height: "60vh",
        boxShadow: "none",
      }}
    >
      <CardHeader title="Welcome!" />
      <CardContent sx={{ overflowY: "scroll", maxHeight: "40vh" }}>
        <p>
          You are invited to take part in a Brown University research study.
          Your participation is voluntary.
        </p>
        <p>
          RESEARCHER: Roman Feiman, PhD, roman_feiman@brown.edu, 190 Thayer St,
          Providence, RI 02912
        </p>
        <p>
          PURPOSE: The study is about how people understand language. We are
          interested in what adult behavior is like so we can find out how
          language comprehension and production change as a person develops.
        </p>
        <p>
          PROCEDURES: In this survey, you will be asked look at a visual display
          and locate particular objects in response to what you read or hear.
        </p>
        <p>
          TIME INVOLVED: The study will take approximately 20 minutes of your
          time.
        </p>
        <p>
          COMPENSATION: The amount of compensation you will receive depends on
          the criterion and the length of the study, in keeping with the
          averages and standards used in the Amazon MechanicalTurk community.
        </p>
        <p>
          RISKS: There is no known or anticipated risk associated with your
          participation in this study. You may discontinue this study at any
          time.
        </p>
        <p>
          BENEFITS: There is no anticipated direct benefit from being in this
          research study.
        </p>
        <p>
          CONFIDENTIALITY: Information gathered in these studies will remain
          strictly confidential and published reports will not mention
          individuals by name. If shared, data will be identified solely by
          anonymized numerical codes. Because participants are recruited through
          Amazon&rsquo;s Mechanical Turk website, participation happens over the
          internet and we have no information about participants&rsquo;
          identities. We collect Worker ID numbers through Amazon Mechanical
          Turk. These will be stored electronically on password-protected
          computers, to which only authorized researchers will have access.
          Following standard practice, we will record IP address to check
          whether a particular internet connection is being used an unreasonable
          number of times for the same experiment. This information will only be
          used to exclude multiple datasets generated from the same IP address.
          Brown University staff sometimes review studies like this one to make
          sure they are being done safely and correctly. If a review of this
          study takes place, your records may be examined. The reviewers will
          protect your confidentiality.
        </p>
        <p>
          VOLUNTARY: You do not have to be in this study if you do not want to
          be. Even if you decide to be in this study, you can change your mind
          and stop at any time.
        </p>
        <p>
          CONTACT INFORMATION: If you have any questions about your
          participation in this study, you can call Roman Feiman at 401-863-6860
          or email Roman_Feiman@Brown.edu
        </p>
        <p>
          YOUR RIGHTS: If you have questions about your rights as a research
          participant, you can contact Brown University&rsquo;s Human Research
          Protection Program at 401-863-3050 or email them at IRB@Brown.edu.
        </p>
        <p>
          IMPORTANT INFORMATION ABOUT YOUR MTURK ID: This ID does not directly
          identify you, but it can be linked to your public profile page. You
          may, therefore, wish to restrict what information you share on this
          public profile. We will not share your mTurk ID with anyone outside of
          our research team. If you ever contact us, Amazon.com will
          automatically insert your email address into the message so that we
          can reply to you. We will use your name and email only to respond to
          your communication and will never distribute it to anyone outside of
          our research team. For more information about the privacy and
          confidentiality limitations associated with using mTurk please refer
          to Amazon&rsquo;s mTurk Privacy Policy:{" "}
          <a
            target="_blank"
            rel="noreferrer noopener"
            href="https://www.mturk.com/mturk/privacynotice"
          >
            https://www.mturk.com/mturk/privacynotice
          </a>{" "}
          and{" "}
          <a
            target="_blank"
            rel="noreferrer noopener"
            href="https://www.mturk.com/mturk/contact"
          >
            https://www.mturk.com/mturk/contact
          </a>{" "}
          .
        </p>
        <p>
          CONSENT TO PARTICIPATE: Clicking the button below confirms that you
          have read and understood the information in this document, are 18
          years old or older and that you agree to volunteer as a research
          participant for this study.
        </p>
        <p>
          You can{" "}
          <NavLink
            to="#"
            onClick={(e) => {
              window.print();
              e.preventDefault();
            }}
          >
            print
          </NavLink>{" "}
          a copy of this form
        </p>
      </CardContent>
      <CardActions
        sx={{
          justifyContent: "space-around",
          alignItems: "center",
          height: "5vh",
        }}
      >
        {/* Buttons using history to switch page at click 
            The page switch is done via React Router using the components in the MainPage.jsx */}
        <Button
          onClick={() => history.push("/")}
          variant="contained"
          size="medium"
          sx={{ displayPrint: "none" }}
        >
          BACK
        </Button>
        <Button
          onClick={() => history.push("/instructions")}
          variant="contained"
          size="medium"
          sx={{ displayPrint: "none" }}
        >
          CONTINUE
        </Button>
      </CardActions>
    </Card>
  );
};

export default Inform;
